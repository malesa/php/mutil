<?php

namespace Mutil;

use PHPMailer\PHPMailer\PHPMailer;

class Mailer
{
   protected $email_from;
   protected $mail = null;

   public function __construct($params = null)
   {
      $this->mail = new PHPMailer(true);

      // Workaround for strict SSL verification and problems with certificates
      $this->mail->SMTPOptions = array(
         'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
         ),
      );

      //Server default settings
      //$mail->SMTPDebug = 2; // Enable verbose debug output
      $this->mail->isSMTP(); // Set mailer to use SMTP
      $this->mail->CharSet = 'UTF-8';
      $this->mail->SMTPAuth = true; // Enable SMTP authentication
      //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

      // Parse additional parameters or overwrite defaults
      if (isset($params) && is_array($params))
      {
         foreach ($params as $param => $val)
         {
            TRACE("Mailer param: $param=$val");
            $this->mail->{$param} = $val;
         }
      }
   }

   protected function getAddressAndName($param)
   {
      $address = '';
      $name = '';
      if (isset($param))
      {
         if (is_array($param))
         {
            $address = $param[0];
            if (count($param) == 2)
            {
               $name = $param[1];
            }
            else
            {
               $name = $param[0];
            }

         }
         else
         {
            $address = $param;
            $name = $param;
         }
      }

      return array($address, $name);
   }

   public function send($to, string $subject, string $body, $from)
   {
      try
      {
         $this->mail->isHTML(true); // Set email format to HTML
         $body = str_replace("\r\n", "<br/>", $body); // Line breaks for HTML format

         // Setting From address and name
         $arr = $this->getAddressAndName($from);
         TRACE($arr);
         $this->mail->setFrom($arr[0], $arr[1]);

         // Add a recipient
         $arr = $this->getAddressAndName($to);
         TRACE($arr);
         $this->mail->addAddress($arr[0], $arr[1]);

         $this->mail->Subject = $subject;
         $this->mail->Body = $body;
         //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

         $this->mail->send();
         return true;
      }
      catch (Exception $e)
      {
         TRACE($this->mail->ErrorInfo);
         trigger_error('Mailer Error: ' . $this->mail->ErrorInfo, E_USER_ERROR);
         return false;
      }
   }
}
