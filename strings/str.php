<?php

namespace Mutil;

class Str
{
    const RE_EMAIL = '/^[a-zA-Z0-9.\-_]+@[a-zA-Z0-9\-.]+\.[a-zA-Z]{2,}$/';

    // Returns first $len characters
    static function left(string $str, int $len)
    {
        if(strlen($str) < $len)
            return '';
        else
            return substr($str, 0, $len);
    }

    // Truncates $len characters from the beginning
    static function cutLeft(string $str, int $len)
    {
        $str_len = strlen($str);
        if($str_len < $len)
            return '';
        else
            return substr($str, $len, $str_len - $len);
    }

    // Returns last $len characters
    static function right(string $str, int $len)
    {
        $str_len = strlen($str);
        if($str_len < $len)
            return '';
        else
            return substr($str, $str_len-$len, $len);
    }

    // Truncates $len characters from the end
    static function cutRight(string $str, int $len)
    {
        $str_len = strlen($str);
        if($str_len < $len)
            return '';
        else
            return substr($str, 0, $str_len-$len);
    }

}
