<?php

namespace Mutil;

class Url
{
   public const NA = 'N/A';

   public static function getServerVal(string $name)
   {
      $val = Url::NA;
      if (isset($_SERVER[$name]))
      {
         $val = $_SERVER[$name];
      }
      return $val;
   }

   public static function getProtocol()
   {
      $protocol = 'N/A';
      if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
         isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
      {
         $protocol = 'https://';
      }
      else
      {
         $protocol = 'http://';
      }

      return $protocol;
   }

   public static function getServerName()
   {
      return Url::getServerVal('SERVER_NAME');
   }

   public static function getServerPort()
   {
      return Url::getServerVal('SERVER_PORT');
   }

   public static function getDocumentRoot()
   {
      // Main document folder on the server
      // C:/server/apache/htdocs
      return Url::getServerVal('DOCUMENT_ROOT');
   }

   public static function getProjectRoot(string $project = null)
   {
      // Project main document folder on the server
      // C:/server/apache/htdocs/myapp
      $val = Url::getServerVal('DOCUMENT_ROOT');
      if (!empty($project))
      {
         $val .= '/' . $project;
      }
      return $val;
   }

   public static function getScriptFileName()
   {
      // Full path to script file
      // C:/server/apache/htdocs/Second/mutil/logger/test.php
      return Url::getServerVal('SCRIPT_FILENAME');
   }

   public static function getScriptBaseName()
   {
      // Script base file
      // test.php
      return basename(Url::getServerVal('SCRIPT_FILENAME'));
   }

   public static function getRequestUri()
   {
      // The URI which was given in order to access this page; for instance:
      // /second/mutil/logger/test.php/data?param=123
      return Url::getServerVal('REQUEST_URI');
   }

   public static function getScriptName()
   {
      // Contains the current script's path. If URI /second/mutil/logger/test.php/data?param=123 then
      // /second/mutil/logger/test.php
      return Url::getServerVal('SCRIPT_NAME');
   }

   public static function getPhpSelf()
   {
      // Contains the current script's path. If URI /second/mutil/logger/test.php/data?param=123 then
      // /second/mutil/logger/test.php/data
      return Url::getServerVal('PHP_SELF');
   }

   public static function getQueryString()
   {
      // If the URL was: http://localhost/second/mutil/logger/test.php?param=123 then
      // param=123
      return Url::getServerVal('QUERY_STRING');
   }

   public static function getPathInfo()
   {
      // If the URL was: http://localhost/second/mutil/logger/test.php/data?param=123 then
      // /data
      return Url::getServerVal('PATH_INFO');
   }

   public static function getFullUrl()
   {
      // Full address
      // http://localhost:80/second/mutil/logger/test.php
      $val = Url::getProtocol() . Url::getServerName() . ':' . Url::getServerPort() . Url::getRequestUri();
      return $val;
   }

   public static function getRequestMethod()
   {
      // Eg POST, GET etc.
      return Url::getServerVal('REQUEST_METHOD');
   }
}
