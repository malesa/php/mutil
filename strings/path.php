<?php
namespace Mutil;

class Path
{
    public static function appendDirSep(string $path, $sep = '/', $endings='/\\')
    {
        $ret = $path;
        $len = strlen($path);
        if ($len > 0 && strpos($endings, $path[$len - 1]) === false) {
            $ret .= $sep;
        }

        return $ret;
    }
}
