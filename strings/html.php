<?php

namespace Mutil;

class Html
{
   public static function printLine($text)
   {
      print($text . "<br />" . PHP_EOL);
   }
   public static function printRule()
   {
      print(PHP_EOL . "<hr />" . PHP_EOL);
   }

   public static function bsAlert($type, string $message, string $title='', bool $title_header=false)
   {
      return BsAlert::getHtml($type, $message, $title, $title_header);
   }
}

class BsAlert
{
   public $type;
   public $message;
   public $title;
   public $title_header;

   function __construct($type, string $message, string $title='', bool $title_header=false)
   {
      $this->type = $type;
      $this->message = $message;
      $this->title = $title;
      $this->title_header = $title_header;
   }

   public function get()
   {
      return self::getHtml($this->type, $this->message, $this->title, $this->title_header);
   }
   
   public static function getHtml($type, string $message, string $title='', bool $title_header=false)
   {
      $header_begin = '<strong>';
      $header_end = '</strong>&nbsp;&nbsp;';
      
      if($title_header)
      {
         $header_begin = '<h4 class="alert-heading">';
         $header_end = '</h4>';
      }
      
      $html = <<<EOT
      <div class="alert alert-$type" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
         $header_begin$title$header_end$message
      </div>
EOT;
      return $html;
   }

}
