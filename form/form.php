<?php

namespace Mutil;

class Form
{
   public static function getVal(string $name, string $default = '')
   {
      $val = $default;
      if (isset($_POST) && array_key_exists($name, $_POST))
      {
         $val = htmlspecialchars($_POST[$name]);
      }
      elseif(isset($_GET) && array_key_exists($name, $_GET))
      {
         $val = htmlspecialchars($_GET[$name]);
      }
      return $val;
   }

   public static function getFiles()
   {
      $files = array();

      if (!isset($_FILES) || count($_FILES) == 0)
      {
         return 0;
      }

      foreach ($_FILES as $file)
      {
         $file_obj = new FormFile($file['name'], $file['size'], $file['type'], $file['tmp_name'], $file['error']);
         array_push($files, $file_obj);
      }

      return $files;
   }
}
