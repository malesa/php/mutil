<?php

namespace Mutil;

class FormFile
{
   protected $name;
   protected $size;
   protected $type;
   protected $tmp_name;
   protected $error_code;
   protected $error_msg;

   function __construct(string $name, $size, string $type, string $tmp_name, $error_code)
   {
      $this->name = $name;
      $this->size = $size;
      $this->type = $type;
      $this->tmp_name = $tmp_name;
      $this->error_code = $error_code;

      $this->error_msg = self::getErrorMessage($error_code);
   }

   static function getErrorMessage(int $error_code)
   {
      $message = '';

      if ($error_code > 0)
      {
         switch ($error_code)
         {
            // jest większy niż domyślny maksymalny rozmiar,
            // podany w pliku konfiguracyjnym
            case 1:   {
                  $message = 'Rozmiar pliku jest zbyt duży.';
                  break;}

            // jest większy niż wartość pola formularza
            // MAX_FILE_SIZE
            case 2:   {
                  $message = 'Rozmiar pliku jest zbyt duży.';
                  break;}

            // plik nie został wysłany w całości
            case 3:   {
                  $message = 'Plik wysłany tylko częściowo.';
                  break;}

            // plik nie został wysłany
            case 4:   {
                  $message = 'Nie wysłano żadnego pliku.';
                  break;}

            // pozostałe błędy
            default:   {
                  $message = 'Wystąpił błąd podczas wysyłania.';
                  break;}
         }
      }

      return $message;
   }

   /**
    * Get the value of name
    */ 
   public function getName()
   {
      return $this->name;
   }

   /**
    * Get the value of size
    */ 
   public function getSize()
   {
      return $this->size;
   }

   /**
    * Get the value of type
    */ 
   public function getType()
   {
      return $this->type;
   }

   /**
    * Get the value of tmp_name
    */ 
   public function getTmpName()
   {
      return $this->tmp_name;
   }

   /**
    * Get the value of error_msg
    */ 
   public function getErrorMsg()
   {
      return $this->error_msg;
   }
}
