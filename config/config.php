<?php

namespace Mutil;

class AppConfig
{
    protected $ini_array;
    protected $sections;

    public function init(string $file_name, bool $sections = true)
    {
        $this->sections = $sections;
        $this->ini_array = parse_ini_file($file_name, $sections);
    }

    public function get(string $param, $defval = null, string $section = null)
    {
        if (!isset($this->ini_array)) {
            throw new \Exception("The configuration file is not loaded", 1);
        }

        if (!$this->sections) {
            if (array_key_exists($param, $this->ini_array)) {
                return $this->ini_array[$param];
            } else {
                return $defval;
            }

        } elseif ($section != null) {
            if (!array_key_exists($section, $this->ini_array)) {
                throw new \Exception("INI section doesn't exist", 2);
            }

            if (array_key_exists($param, $this->ini_array[$section])) {
                return $this->ini_array[$section][$param];
            } else {
                return $defval;
            }
        } else {
            foreach ($this->ini_array as $section => $sect_items) {
                if (array_key_exists($param, $sect_items)) {
                    return $sect_items[$param];
                }
            }
            return $defval;
        }
    }
}
