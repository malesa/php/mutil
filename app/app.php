<?php

namespace Mutil;

class App
{
   protected $session;
   protected $config;
   // protected $logger;
   protected $data_folder;

   private $app_name;
   private $config_file_name;
   private $config_file_ext;
   private $config_folder;
   private $config_path;

   private $log_folder;
   private $log_file_name;
   private $log_file_ext;
   private $log_path;

   public function __construct(string $app_name, $params = null)
   {
      $this->session = new Session();
      $this->app_name = $app_name;
      $this->app_folder = $app_name;
      $this->data_folder = 'data';

      // Default values
      $this->config_folder = 'conf';
      $this->config_file_name = $app_name;
      $this->config_file_ext = 'conf';

      $this->log_folder = 'logs';
      $this->log_file_name = $app_name . '_' . date('Ymd');
      $this->log_file_ext = 'log';

      // Parse parameters
      if (isset($params) && is_array($params))
      {
         foreach ($params as $param => $val)
         {
            $this->{$param} = $val;
         }
      }

      $this->config_path = App::makeConfigPath($this->config_file_name, $this->config_folder, $this->config_file_ext);
      $this->config = new AppConfig();
      $this->config->init($this->config_path);

      $this->app_folder = $this->config->get('app_dir', $this->app_folder, 'general');

      $this->log_folder = $this->config->get('log_dir', $this->log_folder, 'general');
      Logger::init($this->app_name, App::makeLogFolder($this->log_folder), $this->log_file_name . '.' . $this->log_file_ext);

      $this->data_folder = $this->config->get('data_dir', $this->data_folder, 'general');
      $this->data_folder = $this->makeDataFolder($this->data_folder, $app_name);
   }

   public function getSession()
   {
      return $this->session;
   }

   public function getConfig()
   {
      return $this->config;
   }

   public function getLogger()
   {
      return Logger::getLogger();
   }

   public function getDataFolder()
   {
      return $this->data_folder;
   }

   public function getAppFolder()
   {
      return $this->app_folder;
   }
   public function getProjectRoot()
   {
      $dir = Url::getDocumentRoot();
      $dir = Path::appendDirSep($dir);
      return $dir . $this->getAppFolder();
   }

   protected static function makeConfigPath(string $file_name, string $folder = 'conf', string $file_ext = 'conf')
   {
      $dir = Url::getDocumentRoot();
      $dir = dirname($dir) . '/' . $folder . '/' . $file_name . '.' . $file_ext;
      return $dir;
   }

   protected static function makeLogFolder(string $folder = 'logs')
   {
      $dir = '';
      // Check if beginning with '/' or '<letter>:'
      if(strlen($folder) >= 2 && $folder[0] != '/' && $folder[1] != ':')
      {
         // If $folder is not a full path then it's relative path - add it to the document root
         $dir = Url::getDocumentRoot();
         $dir = dirname($dir) . '/' . $folder;
      }
      else
      {
         // If $folder is a full path then do nothing more - return it
         $dir = $folder;
      }
      return $dir;
   }

   protected function makeDataFolder(string $folder = 'data', string $app_name = null)
   {
      $dir = '';
      // Check if beginning with '/' or '<letter>:'
      if(strlen($folder) >= 2 && $folder[0] != '/' && $folder[1] != ':')
      {
         // If $folder is not a full path then it's relative path - add it to the document root
         $dir = Url::getDocumentRoot();
         $dir = dirname($dir) . '/' . $folder;
      }
      else
      {
         // If $folder is a full path then do nothing more - return it
         $dir = $folder;
      }

      if (!empty($app_name))
      {
         $dir = Path::appendDirSep($dir);
         $dir .= $app_name;
      }

      return $dir;
   }

}
