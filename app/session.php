<?php

namespace Mutil;

class Session
{
   function __construct()
   {
      session_start();
   }

   function getVal(string $name, $default = '')
   {
      if(array_key_exists($name.'_type', $_SESSION))
      {
         $type = $_SESSION[$name.'_type'];
         $val = NULL;
         if($type === 'object')
            $val = unserialize($_SESSION[$name]);
         else
            $val = $_SESSION[$name];
         return $val;
      }
      else
      {
         return $default;
      }
   }

   function setVal($name, $val)
   {
      $_SESSION[$name.'_type'] = gettype($val);
      if(is_object($val))
         $_SESSION[$name] = serialize($val);
      else
         $_SESSION[$name] = $val;
   }
};
