<?php

namespace Mutil;

class Debug
{
   const ARRAY_LEFT = '[';
   const ARRAY_RIGHT = ']';
   
   public static function object2array($object, $assoc = true, $empty = '')
   {
      $out = [];

      if (!empty($object))
      {
         $reflector = new \ReflectionObject($object);
         $nodes = $reflector->getProperties();
         //print_r($nodes);
         foreach ($nodes as $node)
         {
            $nod = $reflector->getProperty($node->getName());
            $nod->setAccessible(true);
            $out[$node->getName()] = $nod->getValue($object);
         }
      }
      return $out;
   }

   public static function object2string($var)
   {
      $out = '';

      if(is_object($var))
      {
         $array = self::object2array($var);
         $out = self::array2string($array);
      }

      return $out;
   }

   public static function array2string($var)
   {
      $out = '';

      if(is_array($var))
      {
         $out .= self::ARRAY_LEFT;
         foreach($var as $key => $val)
         {
            $out .= "$key=>";
            if(is_string($val))
               $out .= "'$val'";
            elseif(is_array($val))
               $out .= self::array2string($val);
            elseif(is_object($val))
               $out .= self::object2string($val);
            else
               $out .= "$val";
            $out .= ', ';
         }
         
         $out = Str::cutRight($out, 2);
         $out .= self::ARRAY_RIGHT;
      }

      return $out;
   }

   public static function var2string($var, string $separator=',', string $prefix='', string $suffix='')
   {
      $out = $prefix;

      if(isset($var))
      {
         if(is_array($var))
         {
            $out .= self::array2string($var);
         }
         elseif(is_object($var))
         {
            $out .= self::object2string($var);
         }
         else
         {
            $out .= $var;
         }
      }
      else
      {
         $out .= 'N/A';
      }

      $out .= $suffix;
      return $out;
   }

}
