<?php

function TRACE($message)
{
   if (defined('DEBUG') && DEBUG === true)
   {
      $output = Mutil\Debug::var2string($message, ', ');
      //$output = htmlentities($output);
      $output = addslashes($output);
      $output = str_replace("\r\n", " ", $output);
      $output = str_replace("\n", " ", $output);
      echo ("<script>console.log('$output')</script>");
   }
}
