<?php
namespace Mutil;

$G_logger = null;
class Logger
{
   protected $old_error_handler;
   protected $log_folder;
   protected $log_file;
   protected $log_path;
   protected $app_name;

   public static function getLogger(): ?object
   {
      global $G_logger;
      return $G_logger;
   }

   public static function init($app_name, $log_folder = '../logs/', $log_file = '')
   {
      global $G_logger;

      if (!isset($G_logger))
      {
         $G_logger = new Logger();
      }

      //error_reporting(DEBUG ? E_ALL : 0);

      if ($log_file === '')
      {
         $G_logger->log_file = $app_name . date("_Y-m-d") . '.log';
      }
      else
      {
         $G_logger->log_file = $log_file;
      }

      $G_logger->log_folder = $log_folder;
      $G_logger->app_name = $app_name;
      $G_logger->log_path = Path::appendDirSep($G_logger->log_folder) . $G_logger->log_file;

      // echo $G_logger->log_path;

      $old_error_handler = set_error_handler('Mutil\Logger::errorHandler');
   }

   public static function errorHandler($errno, $errmsg, $filename, $linenum, $vars=null)
   {
      // timestamp for the error entry
      $dt = date("Y-m-d H:i:s");

      // define an assoc array of error string
      // in reality the only entries we should
      // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
      // E_USER_WARNING and E_USER_NOTICE
      $errortype = array(
         E_ERROR => 'Error',
         E_WARNING => 'Warning',
         E_PARSE => 'Parsing Error',
         E_NOTICE => 'Notice',
         E_CORE_ERROR => 'Core Error',
         E_CORE_WARNING => 'Core Warning',
         E_COMPILE_ERROR => 'Compile Error',
         E_COMPILE_WARNING => 'Compile Warning',
         E_USER_ERROR => 'User Error',
         E_USER_WARNING => 'User Warning',
         E_USER_NOTICE => 'User Notice',
         E_STRICT => 'Runtime Notice',
         E_RECOVERABLE_ERROR => 'Catchable Fatal Error',
      );

      //self::saveToSyslog($dt, $errno, $errortype[$errno], $errmsg, $filename, $linenum, $vars);
      self::saveToCSV($dt, $errno, $errortype[$errno], $errmsg, $filename, $linenum, $vars);
      self::saveToConsole($dt, $errno, $errortype[$errno], $errmsg, $filename, $linenum, $vars);
   }

   protected static function saveToCSV($dt, $errno, $errortype, $errmsg, $filename, $linenum, $vars)
   {
      $err = implode('|', array($dt, $errortype, $errmsg, $filename . '(' . $linenum . ')')) . "\n";
      error_log($err, 3, Logger::getLogger()->getLogPath());
   }

   public static function saveDebug($errmsg, $filename, $linenum, $vars=null)
   {
      $dt = date("Y-m-d H:i:s");
      $vars_s = '';
      if(isset($vars))
      {
         if(is_array($vars))
            $vars_s = implode(',', $vars);
         else
            $vars_s = $vars;
      }
      $err = implode('|', array($dt, 'Debug', $errmsg, $filename . '(' . $linenum . ')'));
      if(!empty($vars_s))
         $err .= '|'.$vars_s;
      $err .= "\n";
      

      error_log($err, 3, Logger::getLogger()->getLogPath());
   }

   protected static function saveToSyslog($dt, $errno, $errortype, $errmsg, $filename, $linenum, $vars)
   {
      $err = implode('|', array($dt, $errortype, $errmsg, $filename . '(' . $linenum . ')'));
      error_log($err);
      //syslog(LOG_ERR, $err);
   }

   protected static function saveToConsole($dt, $errno, $errortype, $errmsg, $filename, $linenum, $vars)
   {
      $err = implode('|', array($dt, $errortype, $errmsg, $filename . '(' . $linenum . ')'));
      
      //$err = htmlentities($err);
      $err = addslashes($err);
      $err = str_replace("\r\n", " ", $err);
      $err = str_replace("\n", " ", $err);

      echo ("<script>console.log('$err')</script>");
      
   }

   protected static function saveToXML($dt, $errno, $errortype, $errmsg, $filename, $linenum, $vars)
   {
      // set of errors for which a var trace will be saved
      $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);

      $err = "<errorentry>\n";
      $err .= "\t<datetime>" . $dt . "</datetime>\n";
      $err .= "\t<errornum>" . $errno . "</errornum>\n";
      $err .= "\t<errortype>" . $errortype . "</errortype>\n";
      $err .= "\t<errormsg>" . $errmsg . "</errormsg>\n";
      $err .= "\t<scriptname>" . $filename . "</scriptname>\n";
      $err .= "\t<scriptlinenum>" . $linenum . "</scriptlinenum>\n";

      if (in_array($errno, $user_errors))
      {
         $err .= "\t<vartrace>" . serialize($vars) . "</vartrace>\n";
      }
      $err .= "</errorentry>\n\n";

      error_log($err, 3, Logger::getLogger()->getLogPath());
   }

   /**
    * Get the value of log_path
    */
   public function getLogPath()
   {
      return $this->log_path;
   }
}
