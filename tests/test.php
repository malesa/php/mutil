<?php
namespace Mutil;

define('DEBUG', true);
define('APP_NAME', 'mutil');

require_once '../all.php';

$app = new App(APP_NAME);

Logger::init('mutil', 'logs');

function PrintVarS($name)
{
   if (!isset($_SERVER[$name]))
   {
      Html::PrintLine($name . ": N/A");
   }
   else
   {
      Html::PrintLine($name . ": " . $_SERVER[$name]);
   }

}

function PrintVarS2()
{
   foreach ($_SERVER as $name => $value)
   {
      if (!isset($_SERVER[$name]))
      {
         Html::PrintLine($name . ": N/A");
      }
      else
      {
         Html::PrintLine($name . ": " . $value);
      }

   }
}

PrintVarS2();

Html::printRule();
Html::printLine('getProtocol: ' . Url::getProtocol());
Html::printLine('getServerName: ' . Url::getServerName());
Html::printLine('getServerPort: ' . Url::getServerPort());
Html::printLine('getDocumentRoot: ' . Url::getDocumentRoot());
Html::printLine('getProjectRoot(second): ' . Url::getProjectRoot('second'));
Html::printLine('getProjectRoot: ' . Url::getProjectRoot(''));
Html::printLine('getScriptFileName: ' . Url::getScriptFileName());
Html::printLine('getRequestUri: ' . Url::getRequestUri());
Html::printLine('getFullUrl: ' . Url::getFullUrl());
Html::printLine('getQueryString: ' . Url::getQueryString());
Html::printLine('getPathInfo: ' . Url::getPathInfo());
Html::printLine('getScriptName: ' . Url::getScriptName());
Html::printLine('getPhpSelf: ' . Url::getPhpSelf());
Html::printLine('getRequestMethod: ' . Url::getRequestMethod());

Html::printRule();

$var = array(1,2,'ABC',null);
Html::printLine(Debug::var2string($var));
TRACE($var);

$var = array(1,2,'ABC',array('x', 'y', 'z'));
Html::printLine(Debug::var2string($var));
TRACE($var);


Html::printLine(Debug::var2string($app));
Html::printRule();
//print_r($app);
TRACE($app->getConfig());


trigger_error("Incorrect parameters, string expected", E_USER_ERROR);
